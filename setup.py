# !/usr/bin/env python

from setuptools import setup

setup(
    name='PyAsst',
    version='0.1',
    description='PyAsst is a simple, lightweight Python framework for rapid development.',
    author='Singu',
    author_email='singu@singu.top',
    packages=('PyAsst', 'PyAsst.ext')
)
